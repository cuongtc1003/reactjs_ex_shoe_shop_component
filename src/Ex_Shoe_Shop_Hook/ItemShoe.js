import React from "react";

export default function ItemShoe({ shoe, handlePushToCart, handleShowDetail }) {
  return (
    <div className="p-1 col-4">
      <div className="card border-primary ">
        <img
          className="card-img-top"
          style={{ width: "200" }}
          src={shoe.image}
        />
        <div className="card-body">
          <h4 className="card-title">{shoe.price}$</h4>
          <p className="card-text">
            <strong>{shoe.name}</strong>
          </p>
          <button
            onClick={() => {
              handlePushToCart(shoe);
            }}
            className="btn btn-danger"
          >
            Add To Cart
            <i class="fa-solid fa-cart-shopping"></i>
          </button>
          <button
            onClick={() => {
              handleShowDetail(shoe);
            }}
            id="XemDetail"
            className="bg-dark"
            data-toggle="modal"
            data-target="#exampleModal"
          >
            Xem Chi Tiết
          </button>
        </div>
      </div>
    </div>
  );
}
