import React from "react";

export default function CartShoe({
  cartShoe,
  handleDelete,
  handleChangeQuantity,
}) {
  let renderTbody = () => {
    return cartShoe.map((item, index) => {
      return (
        <tr key={index}>
          <td>
            <strong>{item.id}</strong>
          </td>
          <td>{item.name}</td>
          <td>
            <button
              onClick={(id, luaChon) => {
                handleChangeQuantity(item.id, -1);
              }}
              className="btn btn-warning"
            >
              <strong>-</strong>
            </button>
            <strong className="mx-2">{item.soLuong}</strong>
            <button
              onClick={(idShoe, luaChon) => {
                handleChangeQuantity(item.id, 1);
              }}
              className="btn btn-success"
            >
              +
            </button>
          </td>
          <td>
            <img width={70} src={item.image} alt="" />
          </td>
          <td>
            <strong>{item.soLuong * item.price}</strong>
          </td>
          <td>
            <button
              onClick={() => {
                handleDelete(item.id);
              }}
              className="btn btn-secondary"
              style={{ color: "orange", boxShadow: "2px 3px 2px" }}
            >
              Remove
            </button>
          </td>
        </tr>
      );
    });
  };
  return (
    <div>
      <table
        style={{ backgroundColor: "blanchedalmond", marginTop: "50px" }}
        className="table"
      >
        <thead>
          <tr>
            <th>ID</th>
            <th>Name</th>
            <th>Quantity</th>
            <th>Image</th>
            <th>Total</th>
            <th>Action</th>
          </tr>
        </thead>
        <tbody>{renderTbody()}</tbody>
      </table>
    </div>
  );
}
