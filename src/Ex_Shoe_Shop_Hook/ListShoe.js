import React from "react";
import ItemShoe from "./ItemShoe";

export default function ListShoe({
  listShoe,
  handlePushToCart,
  handleShowDetail,
}) {
  return (
    <div>
      <div className="row">
        {listShoe.map((item, index) => {
          return (
            <ItemShoe
              handleShowDetail={handleShowDetail}
              handlePushToCart={handlePushToCart}
              shoe={item}
              key={index}
            />
          );
        })}
      </div>
    </div>
  );
}
