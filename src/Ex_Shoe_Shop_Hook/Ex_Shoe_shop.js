import React, { useState } from "react";
import { data_shoe } from "../Ex_Shoe_Shop_Hook/data_shoe";
import CartShoe from "./CartShoe";
import DetailShoe from "./DetailShoe";

import ListShoe from "./ListShoe";
function Ex_Shoe_shop() {
  const [listShoe, setLS] = useState(data_shoe);
  const [cartShoe, setCarShoe] = useState([]);
  const [detail, setDetail] = useState({});
  //push to cart
  let handlePushToCart = (shoe) => {
    let clonecartShoe = [...cartShoe];
    let index = clonecartShoe.findIndex((item) => {
      return item.id == shoe.id;
    });
    if (index == -1) {
      let newShoe = { ...shoe, soLuong: 1 };
      clonecartShoe.push(newShoe);
    } else {
      clonecartShoe[index].soLuong += 1;
    }
    console.log(clonecartShoe);
    setCarShoe(clonecartShoe);
  };
  //handleDelete
  let handleDelete = (idShoe) => {
    let cloneCart = [...cartShoe];
    let index = cloneCart.findIndex((item) => {
      return item.id == idShoe;
    });
    cloneCart.splice(index, 1);
    setCarShoe(cloneCart);
  };
  //handleChangQuaantity
  let handleChangeQuantity = (idShoe, luaChon) => {
    let cloneCart = [...cartShoe];
    let index = cloneCart.findIndex((item) => {
      return item.id == idShoe;
    });
    cloneCart[index].soLuong += luaChon;
    if (cloneCart[index].soLuong == 0) {
      cloneCart.splice(index, 1);
    }
    setCarShoe(cloneCart);
  };
  //handleShoeDetail
  let handleShowDetail = (shoe) => {
    setDetail(shoe);
  };
  return (
    <div style={{ backgroundColor: "black" }}>
      <h2 style={{ color: "orange", paddingTop: "20px" }}>Shop Shoe</h2>
      <div className="row">
        <div className="col-6  p-5">
          <ListShoe
            handleShowDetail={handleShowDetail}
            handlePushToCart={handlePushToCart}
            listShoe={listShoe}
          />
        </div>
        <div className="col-6">
          <CartShoe
            handleChangeQuantity={handleChangeQuantity}
            handleDelete={handleDelete}
            cartShoe={cartShoe}
          />
        </div>
      </div>
      <DetailShoe detail={detail} />
    </div>
  );
}
export default Ex_Shoe_shop;
