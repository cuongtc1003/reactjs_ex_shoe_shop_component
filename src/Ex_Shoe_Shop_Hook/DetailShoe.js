import React from "react";

export default function DetailShoe({ detail }) {
  return (
    <div>
      <div className="modal" tabIndex={-1} role="dialog" id="exampleModal">
        <div className="modal-dialog" role="document">
          <div className="modal-content">
            <div className="modal-header">
              <h5 className="modal-title">{detail.name}</h5>
              <button
                type="button"
                className="close"
                data-dismiss="modal"
                aria-label="Close"
              >
                <span aria-hidden="true">×</span>
              </button>
            </div>
            <div className="modal-body">
              <img width={150} src={detail.image} alt />
              <div style={{ fontSize: "20px" }}>{detail.name}</div>
              <h2>{detail.price}$</h2>
              <div>{detail.description}</div>
            </div>
            <div className="modal-footer">
              <button
                type="button"
                className="btn btn-secondary"
                data-dismiss="modal"
              >
                Close
              </button>
              <button type="button" className="btn btn-primary">
                Save changes
              </button>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
