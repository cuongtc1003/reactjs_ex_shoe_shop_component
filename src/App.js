import logo from "./logo.svg";
import "./App.css";
import Ex_Shoe_shop from "./Ex_Shoe_Shop_Hook/Ex_Shoe_shop";

function App() {
  return (
    <div className="App">
      <Ex_Shoe_shop />
    </div>
  );
}

export default App;
